<?php
$data = <<<HEREDOC
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>{$page}</title>
<style>
body {
	background-color: #FFFFFF;
	margin: 0px;
}
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: none;
	color: #5570FF;
}
a:active {
	text-decoration: none;
}
#header {
	min-width: 100%;
	padding-top: 15px;
	padding-bottom: 15px;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #eeeeee;
}
nav {
	text-align: center;
}
nav a {
	margin-top: 5px;
	display: inline-block;
	margin-left: 15px;
	font-size: 16px;
	padding: 5px;
	background-color: #cccccc;
}
nav a:first-child {
	margin-left: 0px;
}
nav a:link {
	text-decoration: none;
	color: #111111;
}
nav a:visited {
	text-decoration: none;
	color: #111111;
}
nav a:hover {
	text-decoration: none;
	color: #111111;
	background-color: #eeeeee;
}
nav a:active {
	text-decoration: none;
	color: #111111;
}
h1 {
	text-align: center;
}
#article {
	margin-left: auto;
	margin-right: auto;
	padding-left: 30px;
	padding-right: 30px;
	word-break: break-all;
	min-height: 300px;
}
h2 {
	text-align: center;
}
blockquote {
	margin-left: 25px;
	padding: 5px;
	background-color: #CCCCCC;
}
#footer {
	text-align: center;
	min-width: 100%;
	padding-top: 15px;
	padding-bottom: 15px;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #eeeeee;
}
</style>
</head>
<body>
<div id='header'>
<h1>{$website}</h1>
<nav>
<a href="{$address}/index.html">What is it?</a>
<a href="{$address}/use_it.html">How do I use it?</a>
<a href="{$address}/learn_more.html">Where can I learn more?</a>
</nav>
</div>
<div id='article'>
<h2 id='article_title'>{$page}</h2>
<p>
{$content}
</p>
</div>
<div id='footer'>
<p><a href="https://laws-lois.justice.gc.ca/eng/acts/C-42/Index.html">Copyright &#169</a> {$copyright_holder}; licensed under {$license}.</p>
</div>
</body>
</html>
HEREDOC;
?>