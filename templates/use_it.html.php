<?php
#Template for /www/public/index.html
#Task
##Data
$location = realpath(dirname(__FILE__, 2));
eval(@substr(@file_get_contents("{$location}/evals/global_variables.php"), 5, -2));
$page = 'How do I use it?';
$content = <<<'NOWDOC'
<h3>To set up your website:</h3>
<ul>
<li>Sign up for <a href="https://gitlab.com/users/sign_up">GitLab</a>.</li>
<li>Download, and install <a href="https://git-scm.com/">Git</a> <i>(how you do this varies depending on your operating system)</i>.</li>
<li>Configure Git.</li>
</ul>

<blockquote>git config --global user.name $username</blockquote>
<blockquote>git config --global user.email $email_address</blockquote> <span style='margin-left:25px'><i>(Reminder: Your email address will be public.)</i></span>

<ul>
<li>Create a new repository in your GitLab account by clicking "+", choosing "New Project", navigating to the "Import project" tab, selecting "Repo by URL", and entering the URL of <a href="https://gitlab.com/jamesdanielmarrsritchey/ritchey-website-generator-template-for-gitlab-pages.git">this repository</a>. You'll be prompted to give your repository a name, and description.</li>
</ul>

<ul>
<li>Download your repository.</li>
</ul>

<blockquote>git clone $repository_url</blockquote>

<ul>
<li>Develop your website, and place it in the 'public' folder.</li>
<li>Update the LICENSE and README.md files to reflect your project. You can also safely delete the 'About.txt' file.</li>
<li>Navigate your terminal into the repository folder (eg: cd ~/Git/project-name). Save, and sync your changes.</li>
</ul>

<blockquote>git add .</blockquote>
<blockquote>git commit -m $comment</blockquote>
<blockquote>git push origin master</blockquote> <span style='margin-left:25px'><i>(You'll be prompted for your GitLab login details.)</i></span>

<ul>
<li>Your website will be available at 'https://$username.gitlab.io/$repository_name/' once uploading, and processing has completed.</li>
</ul>

<br>
<h3>To update your website:</h3>
<ul>
<li>Make your changes.</li>
<li>Navigate your terminal into the repository folder (eg: cd ~/Git/project-name). Save your changes locally.</li>
</ul>

<blockquote>git add .</blockquote>
<blockquote>git commit -m $comment</blockquote>

<ul>
<li>Sync your changes with the GitLab repository.</li>
</ul>

<blockquote>git push origin master</blockquote>
<br>
<h3>To delete your website:</h3>
<ul>
<li>Delete the local repository folder from your computer.</li>
<li>Login to your Gitlab account, navigate to the repository, click "Settings", choose "General", select "Expand" in the Advanced section, and click "Remove Project".</li>
</ul>
NOWDOC;
eval(@substr(@file_get_contents("{$location}/evals/html_layout.php"), 5, -2));
##Write data
@file_put_contents("{$location}{$public}/use_it.html", $data);
?>